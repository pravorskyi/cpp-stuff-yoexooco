/*
    Copyright © 2017 Pravorskii Andrii
    This file is part of cpp-stuff-yoexooco.

    cpp-stuff-yoexooco is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cpp-stuff-yoexooco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cpp-stuff-yoexooco.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <unordered_set>

class QSqlDatabase;
class QSqlError;
class QSqlQuery;
class QString;


namespace std {
    class mutex;
}


class SqlHelper {
public:
     SqlHelper(const SqlHelper&) = delete;
     SqlHelper& operator= (const SqlHelper&) = delete;

     //ctor
     SqlHelper (QSqlDatabase& database);
     //dtor
     ~SqlHelper();

     void good();

     static void dumpError(const QSqlError& error);
     static void dumpError(const QSqlQuery& query);

private:
    void init();
    QSqlDatabase& _db;
    bool	  _good:1;
    bool          _initialized:1;

    static std::mutex _mutex;
    static std::unordered_set<QString> _lockedDB;
};
