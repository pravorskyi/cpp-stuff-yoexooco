/*
    Copyright © 2017 Pravorskii Andrii
    This file is part of cpp-stuff-yoexooco.

    cpp-stuff-yoexooco is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cpp-stuff-yoexooco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cpp-stuff-yoexooco.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/sqlhelper.h"
#include <QtCore/QDebug>
#include <QtCore/QMap>
#include <QtCore/QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <mutex>


// Hash-function for QString (for std::unordered_set)
namespace std
{
    template<>
    struct hash<QString>
    {
        inline std::size_t operator()(QString const& s) const { return qHash(s); }
    };
}


std::unordered_set<QString> SqlHelper::_lockedDB;
std::mutex SqlHelper::_mutex;


SqlHelper::SqlHelper(QSqlDatabase& database) :
    _db(database),
    _good(false),
    _initialized(false)
{
    init();
}


SqlHelper::~SqlHelper()
{
    if(!_initialized)
        return;

    if(_good ? !_db.commit() : !_db.rollback())
        dumpError(_db.lastError());

    _mutex.lock();
    _lockedDB.erase(_db.connectionName());
    _mutex.unlock();
}


void SqlHelper::init()
{
    {
        std::lock_guard<std::mutex> lock(_mutex);
        const auto& conn = _db.connectionName ();
        if (_lockedDB.find(conn) != _lockedDB.end())
            return; // contains conn

        _lockedDB.insert(conn);
    }

    if(!_db.transaction()) {
        dumpError(_db.lastError());
        throw std::runtime_error("Could not start transaction");
    }
    _initialized = true;
}

void SqlHelper::good()
{
    _good = true;
}


void SqlHelper::dumpError(const QSqlError& error)
{
    qWarning() << "SQL error        : " << error.text() << '\n'
               << "Driver error     : " << error.driverText() << '\n'
               << "Database error   : " << error.databaseText() << '\n'
               << "Native code error: " << error.nativeErrorCode() << '\n'
               << "Error type (Qt)  : " << error.type() << '\n'
               << "Error number     : " << error.number();
}


void SqlHelper::dumpError(const QSqlQuery& query)
{
    dumpError(query.lastError());
    qWarning() << "Query: " << query.lastQuery()
               << "\nBound values: " << query.boundValues();
}
