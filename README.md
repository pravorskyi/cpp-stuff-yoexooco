## SqlHelper

Provides database transaction lock.

To use the lock, create an instance of it passing a non-const reference to the QSqlDatabase to be guarded. To initialize and start the locking mechanism, call init(). To notify SqlHelper that everything is good and the database shouldn't be rolled back, call good(). Transaction would be either commited or rolled back in class' destructor.

Inspired by LeechCraft::Util::DBLock https://leechcraft.org/doc/core/class_leech_craft_1_1_util_1_1_d_b_lock.html
